"use strict";

var _express = _interopRequireDefault(require("express"));

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var app = (0, _express.default)();
app.use(_express.default.static("public"));
app.get("/test", function (req, res) {
  res.send("hello world");
});
app.listen(3000, function () {
  console.log("app started");
});