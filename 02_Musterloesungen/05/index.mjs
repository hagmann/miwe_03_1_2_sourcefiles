import express from "express";
import path from "path";
import { fileURLToPath } from "url";

const __dirname = path.dirname(fileURLToPath(import.meta.url));

app.get("/users", (req, res) => {
  res.sendFile(path.join(__dirname, "./users.html"));
});

const PORT = 3000;
const app = express();

app.use("/public", express.static("public"));

app.listen(PORT, () => console.log(`Server running on port ${PORT}`));
