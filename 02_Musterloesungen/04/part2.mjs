// Importieren des fs-Moduls für Dateisystemoperationen
import fs from "fs";

// Asynchrone Umgebung erforderlich für 'await', was jedoch hier nicht korrekt ist
// 'await' kann nicht direkt im Top-Level-Scope außerhalb einer async-Funktion verwendet werden
// Der richtige Ansatz ist, eine asynchrone Funktion zu verwenden oder ohne await zu lesen

// Lesefunktion, ohne 'await' im Top-Level-Scope
const input = fs.readFileSync("./day2/input2.txt", { encoding: "utf-8" });

// Aufteilen des gelesenen Textes in ein Array von Zeilen und Konvertieren jeder Zeile in eine Ganzzahl
const lines = input.split("\n").map(element => parseInt(element, 10));

// Durchlaufen des Arrays mit einer dreifach verschachtelten Schleife, um Triplets von Zahlen zu finden
for (let i = 0; i < lines.length; i++) {
  for (let j = i + 1; j < lines.length; j++) {
    for (let k = j + 1; k < lines.length; k++) {
      // Überprüfen, ob die Summe der aktuellen drei Zahlen gleich 2020 ist
      if (lines[i] + lines[j] + lines[k] === 2020) {
        // Ausgabe des Produkts der drei Zahlen
        console.log(lines[i] * lines[j] * lines[k]);
      }
    }
  }
}
