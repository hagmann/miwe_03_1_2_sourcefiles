const _ = require('lodash');

// Verwende lodash, um einen Array zu manipulieren.
const numbers = [10, 5, 100, 2, 1000];
const sortedNumbers = _.sortBy(numbers);

console.log('Original Numbers:', numbers);
console.log('Sorted Numbers:', sortedNumbers);