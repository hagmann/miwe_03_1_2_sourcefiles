const {sum} = require('./utils/calculate.js')
const Calculator = require('./utils/calculator.js')

// Wenn mit exports.Calculator gearbeitet wird, kann per Desctructuring auf diese "Teilmodule" zugegriffen werden.
// const {Calculator} = require('./utils/calculator.js')

console.log(sum(3,4))


const calc = new Calculator();

console.log(`10 + 5 = ${calc.add(10, 5)}`);
console.log(`10 - 5 = ${calc.subtract(10, 5)}`);
console.log(`10 * 5 = ${calc.multiply(10, 5)}`);
console.log(`10 / 5 = ${calc.divide(10, 5)}`);

console.log('History', calc.history)

// Optional: teste die Division durch null
try {
    console.log(calc.divide(10, 0));
} catch (error) {
    console.error(`Fehler: ${error.message}`);
}