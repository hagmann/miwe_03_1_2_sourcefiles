// CommonJS: Es wird ein Objekt zurückgegeben
// module.exports ist, was exportiert wird
// Syntax ist somit module.exports = Calculator
// Demnach ist der Aufruf so: const Calculator = require('./utils/calculator.js')

// Es ist auch so möglich:
// exports = Calculator

class Calculator {
    constructor() {
        this._history = []
    }

    get history() {
        return this._history;
    }

    add(a,b) {
        const result = a+b;
        this._storeHistory('add', a, b, result);
        return result;
    }

    subtract(a,b) {
        const result = a-b;
        this._storeHistory('subtract', a,b,result);
        return result;
    }

    multiply(a,b) {
        const result = a*b;
        this._storeHistory('multiply',a,b,result);
        return result;
    }

    divide(a,b) {
        if(b===0) {
            throw new Error('Division durch null ist nicht erlaubt');
        }
        const result = a/b;
        this._storeHistory('divide',a,b)
        return result;
    }

    _storeHistory(operation, a,b,result) {
        const entry ={ operation, operands: [a,b], result}
        this._history.push(entry);
    }
}

// Möglichkeit 1: "Teilmodul" exportieren (sinnvoll, wenn es mehrere Funktionen sind)
// exports.Calculator = Calculator

// Möglichkeit 2: "Hauptmodul" exportieren
module.exports = Calculator


// Möglichkeit 3: "Return-Objekt definieren"
/*
module.exports = {
    Calculator,
    specialFunction: (a,b) => a*b
}
    */