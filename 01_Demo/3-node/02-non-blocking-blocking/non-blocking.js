// Importieren des FS-Moduls, um Dateisystemoperationen durchzuführen
const fs = require("node:fs");

// Asynchrones Lesen der Datei 'file.md'
// Der Callback wird ausgeführt, sobald der Lesevorgang abgeschlossen ist
fs.readFile("file.md", (err, data) => {
  if (err) throw err; // Wenn ein Fehler auftritt, wird dieser geworfen
  console.log("Inhalt der Datei:", data.toString()); // Ausgabe des Inhalts der Datei in der Konsole
});

// Eine weitere Funktion, die nach dem Start des Lesevorgangs ausgeführt wird
function moreWork() {
  console.log("Weitere Arbeiten werden vor dem Abschluss des Lesevorgangs durchgeführt.");
}

// Aufruf einer Funktion zur Durchführung zusätzlicher Arbeiten
// Diese wird vor dem Abschluss des asynchronen Lesevorgangs ausgeführt
moreWork(); // Wird vor dem Lese-Callback ausgeführt

