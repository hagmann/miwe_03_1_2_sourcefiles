// Importieren des FS-Moduls, um Dateisystemoperationen durchzuführen
const fs = require("node:fs");
// Importieren des Path-Moduls, um Dateipfade zu handhaben und zu manipulieren
const path = require("node:path");

// Synchrones Lesen der Datei 'file.md'
// Dies blockiert den Event-Loop, bis der Dateilesenvorgang abgeschlossen ist
const data = fs.readFileSync("./file.md", "utf8");

// Ausgabe des Inhalts der Datei in der Konsole
console.log("Inhalt der Datei:", data);

// Simulierter blockierender Vorgang (z.B. lange Rechenoperationen)
function blockingOperation() {
    // Diese Schleife blockiert die Ausführung einige Sekunden lang
    const startTime = Date.now();
    let endTime = startTime;
    while (endTime < startTime + 5000) { // 5 Sekunden Blockade
        endTime = Date.now();
    }
    console.log("Blockierende Operation abgeschlossen.");
}

console.log("Starte blockierende Operation...");
blockingOperation(); // Wird aufgerufen und blockiert die Event-Loop

// Zusätzliche Arbeit, die nach der blockierenden Operation durchgeführt wird
function moreWork() {
    console.log("Weitere Arbeit nach der blockierenden Operation.");
}

moreWork(); // Wird nach der blockierenden Operation aufgerufen
