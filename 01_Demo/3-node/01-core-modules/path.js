// Importieren des Path-Moduls, um Dateipfade zu handhaben und zu manipulieren
const path = require("node:path");

// Definition des Bildnamens
let imageName = "bob_smith";

// Erstellen eines Dateipfads unter Verwendung des join-Befehls von path
// __dirname ist ein vordefinierter Wert, der das Verzeichnis des aktuellen Moduls darstellt
let filepath = path.join(__dirname, "/images/useravatars/", imageName, ".png");

// Ausgabe des vollständigen Dateipfads des Bildes in der Konsole
console.log("The file path of the image is", filepath);

// Hinweis auf andere Methoden zur Pfadmanipulation, die in der Node.js-Dokumentation bereitgestellt werden:
// https://nodejs.org/api/path.html
