// Importieren des HTTP-Moduls, um einen Webserver zu erstellen
const http = require("node:http");

// Festlegen des Server-Ports und der Host-Adresse
const port = 3000;
const host = "localhost";

// Erstellen eines HTTP-Servers
const server = http.createServer((req, res) => {
  // Einstellungen des Antwort-Headers mit Status 200 (OK)
  res.writeHead(200);
  // Senden der Antwort an den Client und Beenden der Anfrage
  res.end("My first server!");
});

// Der Server wird angewiesen, auf dem angegebenen Host und Port zu lauschen
server.listen(port, host, () => {
  // Ausgabe in der Konsole, wenn der Server erfolgreich gestartet wurde
  console.log(`Server is running on http://${host}:${port}`);
});
