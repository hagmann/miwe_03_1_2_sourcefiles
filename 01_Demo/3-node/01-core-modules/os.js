// Importieren des OS-Moduls, um Informationen zum Betriebssystem abzurufen
const os = require("node:os");

// Ausgabe der Architektur des Betriebssystems (z.B. 'x64', 'arm')
console.log(os.arch());

// Ausgabe des verfügbaren freien Speichers im System in Bytes
console.log(os.freemem());

// Ausgabe des gesamten Speichers des Systems in Bytes
console.log(os.totalmem());

// Ausgabe der Netzwerkschnittstellen des Systems einschließlich IP-Adressen
console.log(os.networkInterfaces());

// Ausgabe des temporären Verzeichnisses des Systems (z.B. Ort, an dem temporäre Dateien gespeichert werden)
console.log(os.tmpdir());
