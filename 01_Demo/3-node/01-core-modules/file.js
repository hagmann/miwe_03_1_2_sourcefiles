// Importieren des HTTP-Moduls, um einen Webserver zu erstellen
var http = require("node:http");
// Importieren des FS-Moduls, um Dateisystemoperationen durchzuführen
var fs = require("node:fs");

// Erstellen eines HTTP-Servers
http.createServer(function (req, res) {
  try {
    // Verwenden der readFile-Methode des FS-Moduls, um die Datei 'demofile1.html' zu lesen
    fs.readFile("demofile1.html", function (err, data) {
      if (err) {
        // Falls ein Fehler auftritt, wird dieser im catch-Block behandelt
        throw err; // Fehler werfen, um ihn im try-catch-Block zu erfassen
      }
      // Wenn die Datei erfolgreich gelesen wird, senden Sie einen 200-Statuscode und den HTML-Inhalt als Antwort
      res.writeHead(200, { "Content-Type": "text/html" });
      res.write(data);
      res.end(); // Beenden der Antwort
    });
  } catch (err) {
    // Falls ein Fehler aufgetreten ist, senden Sie einen 404-Statuscode und eine Fehlermeldung
    res.writeHead(404, { "Content-Type": "text/plain" });
    res.write("404 Not Found");
    res.end(); // Beenden der Antwort
  }
})
// Der Server wird auf Port 8082 gestartet
.listen(8082, function() {
  // Ausgabe in der Konsole, wenn der Server erfolgreich gestartet wurde
  console.log("Server läuft unter http://localhost:8082");
});





/*
Features:
    Read files
    Create files
    Update files
    Delete files
    Rename files
*/
