import express from "express"; // Importieren des Express-Moduls, um die Webanwendung zu erstellen

import productRoutes from "./routes/product.js"; // Importieren der Routen für Produktoperationen
import cartRoutes from "./routes/cart.js"; // Importieren der Routen für Warenkorboperationen

const app = express(); // Erstellen einer Express-Anwendung
const PORT = process.env.PORT || 3001; // Festlegen des Ports, auf dem der Server laufen soll, entweder aus der Umgebungsvariable oder Standard auf 3001

app.use(express.json()); // Middleware zur Verarbeitung von JSON-Anfragen

app.use("/product", productRoutes); // Verwenden der Produkt-Routen für alle Anfragen, die mit '/product' beginnen
app.use("/cart", cartRoutes); // Verwenden der Warenkorb-Routen für alle Anfragen, die mit '/cart' beginnen

app.use('/', express.static('public'))

// Starten des Servers und Ausgabe einer Meldung in der Konsole
app.listen(PORT, () => {
  console.log(`Server läuft auf Port ${PORT}`);
});
