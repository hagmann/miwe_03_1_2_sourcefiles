import { loadCart, save } from "../helper/cart.js";
import CartItem from "../classes/cartItem.js";

// Initialisieren des Warenkorbs durch Laden der Inhalte aus der Datei
let cart = loadCart();

// Controller zur Verarbeitung des Abrufs des Warenkorbs
export const getCart = (req, res) => {
  res.json(cart);
};

// Funktion zum Hinzufügen eines Produkts zum Warenkorb
export const addToCart = (req, res) => {
  try {
    const { productId, quantity } = req.body;

    if (quantity <= 0) {
      return res.status(400).json({ error: "Die Menge muss größer als 0 sein." });
    }

    const cartEntry = new CartItem(productId, quantity);
    if (!cartEntry.product) {
      return res.status(404).json({ error: "Produkt mit der angegebenen ID nicht gefunden." });
    }

    cart.push(cartEntry);
    save(cart); // Die aktuelle Version des Warenkorbs speichern

    return res.status(201).json({ success: true, data: cartEntry });
  } catch (error) {
    console.error("Fehler beim Hinzufügen zum Warenkorb:", error);
    return res.status(500).json({ error: "Ein Fehler ist aufgetreten." });
  }
};

// Funktion zum Entfernen eines Produkts aus dem Warenkorb
export const removeFromCart = (req, res) => {
  try {
    const { id } = req.params; // Annahme, dass die ID als URL-Parameter gesendet wird
    const foundIndex = cart.findIndex(x => x.id === id);

    console.log(id)

    if (foundIndex !== -1) {
      cart.splice(foundIndex, 1);
      save(cart); // Den aktualisierten Warenkorb speichern
      return res.status(200).json({ success: true, data: cart });
    } else {
      return res.status(404).json({ error: "Kein Warenkorbartikel mit der angegebenen ID gefunden." });
    }
  } catch (error) {
    console.error("Fehler beim Entfernen aus dem Warenkorb:", error);
    return res.status(500).json({ error: "Ein Fehler ist aufgetreten." });
  }
};
