import { v4 as uuidv4 } from "uuid"; // Importieren der uuidv4-Funktion, um eindeutige IDs zu generieren
import { findProductById } from "../helper/product.js"; // Importieren der Hilfsfunktion zum Auffinden eines Produkts nach ID

/**
 * Klasse CartItem repräsentiert einen Artikel im Warenkorb
 * @class
 */
export default class CartItem {
  /**
   * Konstruktor zur Erstellung eines neuen CartItem-Objekts
   * @param {string} productId - Die ID des Produkts, das diesem Warenkorbartikel zugeordnet ist
   * @param {number} quantity - Die Menge des Produkts
   */
  constructor(productId, quantity) {
    this.id = uuidv4(); // Generierung einer eindeutigen ID für den Warenkorbartikel
    this.product = findProductById(productId); // Finden und Zuordnen des Produkts durch die übergebene Produkt-ID. Damit wird das ganze Produkt kopiert, anstelle referenziert. Das ist wichtig, falls sich später z.B. der Preis ändert.
    this.quantity = quantity; // Festlegen der quantitativen Menge für das Produkt
  }
}
