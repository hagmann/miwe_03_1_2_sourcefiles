const productsList = document.getElementById('products');
const cartList = document.getElementById('cart');

function createProductHTML(product) {
    return `
        <div class="product">
            <div class="image">
                <img src="${product.image}" width="100%" />
            </div>
            <div class="product-meta">
                <div class="name">${product.name}</div>
                <div class="price">CHF ${product.price.toFixed(2)}</div>
                <form class="add">
                    <input name="id" type="hidden" value="${product.id}">
                    <input name="quantity" type="number" value="1">
                    <button type="submit">In den Warenkorb</button>
                </form>
            </div>
        </div>`;
}

function createCartItemHTML(item) {
    return `
        <div class="product">
            <img src="${item.product.image}" width="100%" />
            <div class="product-meta">
                <div class="name">${item.quantity}x ${item.product.name}</div>
                <div class="price">CHF ${item.product.price.toFixed(2)}</div>
                <form class="remove">
                    <input name="id" type="hidden" value="${item.id}">
                    <button type="submit">x</button>
                </form>
            </div>
        </div>`;
}

async function loadData() {
    try {
        const response = await fetch('/product');
        const products = await response.json();
        productsList.innerHTML = products.map(createProductHTML).join('');
        updateProductCount(products.length); // Update product count
        attachAddListeners();
    } catch (e) {
        console.error('Fehler beim Laden der Produkte:', e);
    }
}

async function loadCart() {
    try {
        const response = await fetch('/cart');
        const cart = await response.json();
        cartList.innerHTML = cart.map(createCartItemHTML).join('');
        updateCartCount(cart.length); // Update cart count
        attachRemoveListeners();
    } catch (e) {
        console.error('Fehler beim Laden des Warenkorbs:', e);
    }
}

function attachAddListeners() {
    document.querySelectorAll('form.add').forEach(form => {
        form.addEventListener('submit', async (e) => {
            e.preventDefault();
            const id = e.target.elements.id.value;
            const quantity = e.target.elements.quantity.value;
            await addToCart(id, parseInt(quantity));
        });
    });
}

function attachRemoveListeners() {
    document.querySelectorAll('form.remove').forEach(form => {
        form.addEventListener('submit', async (e) => {
            e.preventDefault();
            const id = e.target.elements.id.value;
            await removeFromCart(id);
        });
    });
}

// Function to update the product count
function updateProductCount(count) {
    document.getElementById('product-count').textContent = `(${count})`;
}

// Function to update the cart count
function updateCartCount(count) {
    document.getElementById('cart-count').textContent = `(${count})`;
}


async function addToCart(id, quantity = 1) {
    try {
        await fetch('/cart', {
            method: 'POST',
            headers: { "Content-Type": "application/json" },
            body: JSON.stringify({ productId: id, quantity })
        });
        loadCart();
    } catch (e) {
        console.error('Fehler beim Hinzufügen zum Warenkorb:', e);
    }
}

async function removeFromCart(id) {
    try {
        await fetch(`/cart/${id}`, {
            method: 'DELETE'
        });
        loadCart();
    } catch (e) {
        console.error('Fehler beim Entfernen aus dem Warenkorb:', e);
    }
}


loadData();
loadCart();