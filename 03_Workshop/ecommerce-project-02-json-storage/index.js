import express from "express";
import fs from "fs";

const app = express();
const PORT = process.env.PORT || 3001;
const PRODUCTS_FILE = "data/products.json";

// Funktion zum Lesen der Produkte aus der Datei
function readProductsFromFile() {
  try {
    const data = fs.readFileSync(PRODUCTS_FILE, "utf-8");
    return JSON.parse(data);
  } catch (err) {
    console.error("Fehler beim Lesen der Datei:", err);
    return [];
  }
}

// Funktion zum Schreiben der Produkte in die Datei
function writeProductsToFile(products) {
  try {
    fs.writeFileSync(PRODUCTS_FILE, JSON.stringify(products, null, 2));
  } catch (err) {
    console.error("Fehler beim Schreiben in die Datei:", err);
  }
}

// Initiale Liste von Produkten laden
let products = readProductsFromFile();

// Middleware zum Parsen eingehender JSON-Anfragen
app.use(express.json());

// Middleware für die Fehlbehandlung
app.use((err, req, res, next) => {
  console.error(err.stack); // Fehler in der Konsole ausgeben
  res.status(500).json({ error: "Ein interner Serverfehler ist aufgetreten." });
});

// Root-Route zum Testen des Servers
app.get("/", (req, res) => {
  res.json("Hello World");
});

// Alle Produkte abrufen
app.get("/product", (req, res) => {
  res.json(products);
});

// Ein einzelnes Produkt nach ID abrufen
app.get("/product/:id", (req, res) => {
  const found = products.find(element => element.id == req.params.id);
  if (found) {
    res.json(found);
  } else {
    res.status(404).json({ error: "Produkt nicht gefunden" });
  }
});

// Ein neues Produkt hinzufügen
app.post("/product", (req, res) => {
  const { name } = req.body;

  if (!name) {
    return res.status(400).json({ error: "Name ist erforderlich" });
  }

  const newProduct = { id: products.length + 1, name };
  products.push(newProduct);
  writeProductsToFile(products);
  
  res.status(201).json({ success: true, data: newProduct });
});

// Ein bestehendes Produkt nach ID aktualisieren
app.put("/product/:id", (req, res) => {
  let found = false;
  const { name } = req.body;

  if (!name) {
    return res.status(400).json({ error: "Name ist erforderlich" });
  }

  let newProducts = products.map(element => {
    if (element.id == req.params.id) {
      found = true;
      return { id: element.id, name };
    } else {
      return element;
    }
  });

  if (found) {
    products = newProducts;
    writeProductsToFile(products);
    res.json({ success: true, data: products });
  } else {
    res.status(404).json({ error: "Produkt nicht gefunden" });
  }
});

// Ein Produkt nach ID löschen
app.delete("/product/:id", (req, res) => {
  const lengthBefore = products.length;
  products = products.filter(element => element.id != req.params.id);

  if (products.length < lengthBefore) {
    writeProductsToFile(products);
    res.json({ success: true, data: products });
  } else {
    res.status(404).json({ error: "Produkt nicht gefunden" });
  }
});

// Starten des Servers
app.listen(PORT, () => {
  console.log(`Server läuft auf Port ${PORT}`);
});
